var hslider = {

		build: function() {

			$ww = $window.width();
			$wh = $window.height();
			$(".slider-container").each(function(i, el){
				$this = $(el);
				$this.css({width: $ww, height: ($wh - SECTION_PADDING_TOP - HEADER_HEIGHT - H1_HEIGHT)})
				$slider = $this.find(".sl");
				$slides = $slider.children("li.slide");
				$slides.css({width: $ww, height: ($wh - SECTION_PADDING_TOP - HEADER_HEIGHT - H1_HEIGHT)})
				$slider.css({
					width: $slides.length * $ww
				})

				$nav = $("<ul class='slider-navigator unstyled'>");
				$nav.append('<li class="prev hide"><span class="glyphicon glyphicon-chevron-left"></li>').append('<li class="next"><span class="glyphicon glyphicon-chevron-right"></li>');
				$nav.appendTo($this);

				$slides.first().addClass("active");
				$nav.find("li.prev").click(function(){
					$active = $slides.parent().find(".active");
					$index = $active.index();
					$target = $index - 1;
					if ($target >= 0) {
						$offset = ($target * $ww);
						myCv.hslider.beforeSlide($slides.eq($target));
						myCv.hslider.load_slide($slider, $offset, $slides, $nav);
					}
				})

				$nav.find("li.next").click(function(){
					$active = $slides.parent().find(".active");
					$index = $active.index();
					$target = $index + 1;
					if ($target < $slides.length) {
						$offset = -($target * $ww);
						myCv.hslider.beforeSlide($slides.eq($target));
						myCv.hslider.load_slide($slider, $offset, $slides, $nav);
					}
				})

			})
			
		},
		load_slide: function($slider, $offset, $slides, $nav) {
					TweenMax.to($slider, 1, {left: $offset, onComplete: function(){
									$slides.removeClass("active");
									$current_slide = $slides.eq($target)
									$current_slide.addClass("active");
									myCv.hslider.display_nav($target, $nav, $slides);
									myCv.hslider.afterSlide($current_slide)
								},
		        				ease: Power4.easeInOut
					});
		},
		display_nav: function($target, $nav, $slides) {
			if (($target + 1) ==  $slides.length) {
				$nav.find("li.next").addClass("hide")
			} else {
				$nav.find("li.next").removeClass("hide")
			}

			if ($target == 0) {
				$nav.find("li.prev").addClass("hide")
			} else {
				$nav.find("li.prev").removeClass("hide")
			}
		},
		resize: function() {
			$ww = $window.width();
			$wh = $window.height();
			$(".slider-container").each(function(i, el){
				$this = $(el);
				$this.css({width: $ww, height: ($wh - SECTION_PADDING_TOP - HEADER_HEIGHT - H1_HEIGHT)})
				$slider = $this.find(".sl");
				$slides = $slider.children("li.slide");
				$slides.css({width: $ww, height: ($wh - SECTION_PADDING_TOP - HEADER_HEIGHT - H1_HEIGHT)})
				$slider.css({
					width: $slides.length * $ww
				})
			})
		},
		beforeSlide : function($slide) {
			$action = $slide.data("before");
			if (typeof $action == "undefined") return
			if (typeof myCv.hslider.actions.before[$action] !== 'function')  return;
			myCv.hslider.actions.before[$action];
		},
		afterSlide : function($slide) {
			$action = $slide.data("after");
			if (typeof $action == "undefined") return
			if (typeof myCv.hslider.actions.after[$action] !== 'function')  return;
			myCv.hslider.actions.after[$action]();
		},
		actions: {
			before: [],
			after: []
		}
	}

myCv.hslider.actions.after.lellone = function() {
	TweenMax.to("#working-timeline", 0.5, {width: "100%", height: "auto", marginTop: 0, marginLeft: 0, opacity: 1, delay: 0.5})
}