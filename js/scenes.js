var scenes =  {

	controller : null,
	home_scene : function() {

		var scene = new ScrollScene({triggerElement: "#about"})
					.setTween(TweenMax.to("#main-navbar", 0.5, {opacity: 1, marginTop: 0}));
		return scene;
	},
	about_scene: function() {

		var s = 0;
		var DLY = 50;
		var $screen = document.getElementById("about-me-text");
		var screentval = null;

		var writescreen = function() {
			if ($screen.innerHTML != "") return;
			if (screentval) clearInterval(screentval);
			$screen.innerHTML = "";
            var finalmatrix = [];
            for(var i = 0; i < about_text.length; i++) {
                line = about_text[i];
                finalmatrix = finalmatrix.concat(line.split(""), ["<br/>"]);
            }
            var index = 0;
            var screentval = setInterval(function() {
                    if (index < finalmatrix.length) {
                    	$screen.innerHTML += finalmatrix[index];
                        index++;
                    } else {
                    	
                    	clearInterval(screentval);
                    	setTimeout(turn_me, 300);
                    }
            }, DLY)
		}

		var turn_me = function () {
			$("#about-dany").addClass("front");
			setInterval(function(){
				$("#eyes-closed").removeClass("hide");
				var closetimeout = setTimeout(function() {
					$("#eyes-closed").addClass("hide");
					clearTimeout(closetimeout);
					setTimeout(function(){
						$("#eyes-closed").removeClass("hide");
							setTimeout(function(){
								$("#eyes-closed").addClass("hide");
							}, 80)
					},100)
				},80)
			}, 3500);
			TweenMax.to($("#about .go-to-next-section"), 1, {bottom: "5%", opacity: 1});
		}


		
		var timeline = new TimelineMax();

		var intro = [ 	
						TweenMax.from(".about-me", 2, {opacity:0}),
						TweenMax.from("#about .fake-background.bottom", 1.5, {opacity: 0 , bottom:"-100%", ease:Elastic.easeOut}),
						TweenMax.from("#about .fake-background.top", 1.5, {opacity: 0 , top:"-100%", ease:Elastic.easeOut}),
					]
		timeline.add(intro);

		var desktop = [
						TweenMax.from("#about-pc", 1, {opacity:0, left: "130%", onComplete: writescreen}),		
					  	TweenMax.from("#about-keyboard", 1, {opacity:0, left: "120%"}),
					  	TweenMax.from("#about-dany", 1, {opacity:0, left: "-10%"})
					  ]
		timeline.add(desktop);
		
		var scene = new ScrollScene({triggerElement: "#about"}).setTween(timeline);

		return scene;
	},
	work_scene : function() {

		$(".work-content").css({
			height: $(window).height() - 72,
			width: $(window).width()
		})

		$(".full-screen").css({
			height: $(window).height() - HEADER_HEIGHT,
			width: $(window).width()
		})

		var onFront = function(element) {
			var $win = $(element);
			$(".window").removeClass("front");
			$win.addClass("front");
		}
		$(".window").click(function(){
			onFront(this);
		});

		$("#photoshop, #command-line, #timeline, .desktop-icon").draggable({
			containment: 'parent',
			cursor: 'move',
			delay: 50,
			cancel: '#viewport'
		});

		$("#photoshop, #command-line, #timeline").resizable({
			minHeight: 400,
	      	minWidth: 500
		});
		
		// PHOTOSHOP OPEN / CLOSE
		$("#photoshop > .title-bar .win-close-button, #task-front").click(function(){
			var $ps = $("#photoshop")
			if ($ps.hasClass("closed")) {
				//open
				$ps.removeClass("closed")
				onFront($ps);
				ga('send', 'event', 'work', 'section', 'open-photoshop'); 
			} else {
				$ps.addClass("closed")
			}
		});

		//COMMAND LINE OPEN CLOSE
		$("#command-line .win-close-button, #task-back").click(function(){
			var $cmd_win = $("#command-line")
			if ($cmd_win.hasClass("closed")) {
				//open
				$cmd_win.removeClass("closed")
				$("#cmd").trigger('focus');
				onFront($cmd_win);
				ga('send', 'event', 'work', 'section', 'open-command-line'); 
			} else {
				$cmd_win.addClass("closed")
			}
		});

		//TIMELINE OPEN / CLOSE
		$("#timeline .win-close-button, #task-timeline").click(function(){
			var $tl_win = $("#timeline")
			if ($tl_win.hasClass("closed")) {
				//open
				$tl_win.removeClass("closed")
				onFront($tl_win);
				ga('send', 'event', 'work', 'section', 'open-time-line'); 
			} else {
				$tl_win.addClass("closed")
			}
		});

		//PHOTOSHOP FN

		$("#photoshop-layers ul li").click(function(){
			var $this = $(this);
			var $skills = $("#frontend-document .skills");
			var $item = $skills.find("." + $this.data("item"));

			if($this.hasClass("disabled")) {
				$item.removeClass("hide");
				$this.removeClass("disabled");
			} else {
				$item.addClass("hide");
				$this.addClass("disabled");
			}

			
			
		})

		//COMMAND LINE FN
		var doCommand = function(e) {
	         if(e.which == 13 && $("#cmd").is(":focus")) {
	         	e.preventDefault();
	         	$("#cmd-result").addClass("hid");
	         	var $value = $("#cmd").val();
	         	$("#cmd").val('');
	         	var $result_container = $("#cmd-result");
	         	var $cmd = {};
	         	if (typeof commands[$value] == "undefined") {
	         		$cmd.text = $value + ": command not found";
	         		$cmd.complete = function(){}
	         	} else {
	         		$cmd = commands[$value]
	         	}
	         	$result_container.html($cmd.text).removeClass("hid").slideDown('fast', $cmd.complete());
	         }
		}

		$(window).bind('keypress', doCommand);

		//TIMELINE FN
		var scroll_timeline = function(e){

			var $clicked = $(e.currentTarget);
			var target = $clicked.data("target");
			$target = $(target).first();
			$scrollTop = $target.parent().scrollTop() + $target.offset().top - $target.parent().offset().top
			$("#viewport").stop().animate({
				scrollTop : $scrollTop
			}, 200)
		}

		$("#vp-timeline-list a").click(scroll_timeline);
		
		var viewport_scroll_timeout = null;
		
		$("#viewport").scroll(function(){
			$that = $(this);
			if (viewport_scroll_timeout == null) {
				viewport_scroll_timeout = setTimeout(function() {
				var START = 170;
				var BP = 195;
				var $newtop = ($that.scrollTop() > BP) ? ($that.scrollTop() - BP) : 0;
				$newtop += START;
				$("#vp-timeline-list").stop().animate({top: $newtop}, 200);
				
				clearTimeout(viewport_scroll_timeout);
				viewport_scroll_timeout = null
				}, 100)
				
			}
		})
	}

}

var about_text = [
			"Hi, my name is Daniel.       ",
			" ",
			"I'm a OO PHP developer since 2008.           ",
			"I like also play with the frontend and graphics, as you can see here :)         ",
			" ",
			"Wanna see more?                ",
			"Click the arrow below!     "
		   ];

var commands = {

		help : {
			text : " type <span class='green'>skills</span> to list all my skills\
\n type <span class='green'>cls</span> to clean skill\
\n try <span class='green'>&lt;something&gt;</span> to find an easter egg",
			complete: function(){}
		}
		,
 		skills : {
 			text: 'DEV\
\n    PHP     <span class="score" data-score="100"><span class="percent"></span></span>   100/100\
\n    ZF 1    <span class="score" data-score="100"><span class="percent"></span></span>   100/100\
\n    Propel  <span class="score" data-score="100"><span class="percent"></span></span>   100/100\
\n    MySql   <span class="score" data-score="90"><span class="percent"></span></span>    90/100\
\n    Phpunit <span class="score" data-score="90"><span class="percent"></span></span>    90/100\
\n\
\nPLATFORMS\
\n    Magento <span class="score" data-score="60"><span class="percent"></span></span>    60/100\
\n    WP      <span class="score" data-score="70"><span class="percent"></span></span>    70/100\
\n    Drupal  <span class="score" data-score="65"><span class="percent"></span></span>    65/100\
\n\
\nTOOLS\
\n    GIT     <span class="score" data-score="95"><span class="percent"></span></span>    95/100\
\n    SVN     <span class="score" data-score="77"><span class="percent"></span></span>    77/100\
\n    BA$H    <span class="score" data-score="65"><span class="percent"></span></span>    65/100\
\n    VIM     <span class="score" data-score="72"><span class="percent"></span></span>    72/100',

			complete : function() {
							var $result_container = $("#cmd-result");
							$result_container.find(".percent").css({width: 0});
							$result_container.find(".score").each(function(i, el){
								var $el = $(el);
								var $width = $el.data("score") + "%";
								TweenMax.to($el.find(".percent"), 1, {width: $width})
							})
					}
		},
		cls : {
			text : "",
			complete : function() {}
		},
		rush: {
			text : "Rush 2112\
\n                       ...::::!!::::::...\
\n               .:>-~``       !~ !:       ``~~-:.\
\n          .:-~`             !~   4h             ``~:.\
\n       .<~`                !~     ~!                ``!:\
\n     <~                  .!~       `!.                  ~:.\
\n   <~              !>~~ .!~         `!.                   ~h\
\n::!!::::><<>------<8k:!.!~  :x!`` ``-`~~~~~~~~~~~~~~~~~~~~!!!!\
\n !``~<:.           #$Ni:..  !WX     '~   xX          .:<~`  '!\
\n!>      `~<:.       `#$$$WX:X$WX:  '  `~@$(:    ..:~~`       !\
\n!>           `~<:.    !$$$RUXI?~ '      <$>..:!~`            <\
\n!!               ``!> `M$$$$$$$i      --\"\"`!~                !\
\n !:               <!   ~$$M$R$$R         ~ `!.              :!\
\n  ~:             <!     X$W$N!\"             '!:            <~\
\n   `!:          <~     'R$$$$@$>   <:.        !:         :!`\
\n     `~:.      !~    .:!#$F`#$!    '. `~<:.    ~h     .:~`\
\n        `~:.  !~..>~`    TF   #$$!  ~      `~<..~!..<~`\
\n            `!!!.      .dR\"    #$!           ..!~~!\
\n                 ``~--<!f ..    !M.  ..:>--~`\
\n                       !        `!?\
\n                      !         !!~\
\n                                !!\
\n                                !\
\n                                !    `",
			complete: function() {

			}
		}

}


