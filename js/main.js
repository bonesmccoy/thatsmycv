var SECTION_PADDING_TOP = 25;
var HEADER_HEIGHT =  72;
var H1_HEIGHT = 39;

var $window = $(window);
var slides_actions = [];

var myCv = {

	boot : function() {

		$.cookiesDirective({
			privacyPolicyUri: 'privacy.html',
			//explicitConsent: false,
			position : 'bottom',
			scriptWrapper: cookie_controller, 
			cookieScripts: 'Google Analytics', 
			backgroundColor: '#585594',
			linkColor: '#ffffff'
		});
		myCv.resize();

		$('.navbar-nav a').on('click', function(){ 
			if($('.navbar-header .navbar-toggle').css('display') !='none'){
	            $("#main-menu").collapse( "hide" );
	        }
    	});

		myCv.build_horizontal();
		myCv.animation.load_scenes();
		myCv.build_contact();
				
		//FIRE THE SITE
		myCv.animation.fire_site();
		
		
	},
	resize: function() {
		$("section").css("min-height", $window.height());
	},
	goto_page: function(e) {

		    var $anchor = $(e.currentTarget);
		    var $target = $("#" + $anchor.data("scroll"));
		    myCv.animation.scroll_to_target($target);
		    ga('send', 'event', 'pageview', 'click', $anchor.data("scroll"));
		    e.preventDefault();
	},
	locate: function() {
		var route = window.location.hash.replace("#", "").replace("!", "").replace("/", "");
		var $target = null;
       	if (route) {
	       	$target = $("*[data-route=" + route +"]").first();
	    }
	    ga('send', 'event', 'pageview', 'locate', route); 
        myCv.animation.scroll_to_target($target);
	},
	build_horizontal: function() {
	},
	animation:  {
		scroll_to_target: function($target) {
			var $t = (!!$target && ($target.length > 0)) ? $target : $("body")
		    var $scrollTo = ($t.offset().top);
		    var $route = (!!$t.data("route")) ? $t.data("route") : "";
		   	TweenMax.to($("body, html"), 1, {
		   		scrollTop: $scrollTo,
		   		onComplete: function(){
		            window.location.hash = "#!/" + $route;
		            $section_title = $t.data("title");
		            $("#heresmycv .section-name").html($section_title);
		        },
		        ease: Power4.easeInOut
		    });
		},
		fire_site : function() {
			TweenLite.to('#loading-bar', 0.5, {opacity: 0, height: "50%", top: 0, marginTop: 0});
			TweenLite.to('#the-site', 1, {
							opacity: 1, 
							onComplete:function(){
								$(".goto").click(myCv.goto_page);
								myCv.locate();
								$("#loading-bar").hide();
							}
						});

		},
		load_scenes: function() {

			scenes.controller = new ScrollMagic();

			//navbar animation
			var home = scenes.home_scene()
			home.addTo(scenes.controller);

			var about = scenes.about_scene();
			about.addTo(scenes.controller);

			var work = scenes.work_scene();
			
		}
	},
	build_contact: function() {

		var validateEmail = function (email) { 
		    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		    return re.test(email);
		};

		$.get("ng/_HERES.php", function(data) {
			$("#herenonce").val(data.herenonce);
		});

		$("#send-mail").click(function(){

			if ($("#full_name").val() == "") {
				$("#full_name").addClass("error");
				return;
			}
			if ( $("#email").val()  == "" || !validateEmail($("#email").val()) ) {
				 $("#email").addClass("error");
				return;
			} 

			if ($("#message").val() == "") {
				$("#message").addClass("error");
				return;
			}
				$.post("ng/_ngine.php", $("#contact-me").serialize() ,
						function(){
							ga('send', 'event', 'contact-form', 'click', 'send-mail'); 
							$("#contact-me").fadeOut('fast', function() {
								$("#contact-me input[type=text]").val('');
								$("#contact-me textarea").val("");
								$.get("ng/_HERES.php", function(data) {
									$("#herenonce").val(data.herenonce);
								});
				                $("#contact-me-thank").fadeIn('fast', function() {
				                    setTimeout(function() {
							                     $("#contact-me-thank").fadeOut('slow', function() {
							                     	$("#contact-me").fadeIn('slow');
							                     })
							                   }, '3000');
				                })
            				})
				});

		});

		$("#contact-me input, #contact-me textarea").focus(function() {
			$(this).removeClass("error");
		})
	}
}



$(window).resize(function(){
	myCv.resize();
})