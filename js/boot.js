var assets = [
    "css/bootstrap.min.css",
    "css/bootstrap-theme.min.css",
    "css/ui/jquery-ui.css",
    "css/main.css",
    "js/vendor/jquery.scrollmagic.min.js"
]

var onQueueProgress = function () {
    document.getElementById('loading-bar').style.width = Math.floor(queue.progress * 100) + '%';
}

var onQueueLoaded = function () {
    myCv.boot()
}

var handleFileLoad = function (e) {
    if (e.item.type === 'javascript') {
        //console.log(e.item.src);
        document.getElementsByTagName('body')[0].appendChild(e.result);
    }
    else if (e.item.type === 'css') {
        document.getElementsByTagName('head')[0].appendChild(e.result);
    }
}


var queue = new createjs.LoadQueue(true);

queue.setMaxConnections(1);
queue.addEventListener("fileload", handleFileLoad);
queue.addEventListener("complete", onQueueLoaded);
queue.addEventListener("progress", onQueueProgress);

//load assets 
for (var a = 0; a < assets.length; a++) {
    queue.loadFile( assets[a], false);
}

queue.load();


